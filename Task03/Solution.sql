-- 1
select model, speed, hd from PC where price < 500;
-- 2
select distinct maker from Product where type = 'Printer';
-- 3
select model, ram, screen from Laptop where price > 1000;
-- 4
select * from Printer where color = 'y';
-- 5
select model, speed, hd from PC
where (cd = '12x' or cd = '24x') and price < 600;
-- 6
select distinct maker, speed
from Product join Laptop on Product.model = Laptop.model
where  hd >= 10;
--7
select Product.model,price
from Product join PC on Product.model = PC.model
where maker = 'B'
union
select Product.model,price
from Product join Laptop on Product.model = Laptop.model
where maker = 'B'
union
select Product.model,price
from Product join Printer on Product.model = Printer.model
where maker = 'B';
--8
select maker from product where type = 'PC'
except
select maker from product where type = 'Laptop';
--9
select distinct maker
from Product join PC on Product.model = PC.model
where speed >= 450;
-- 10
select model, price
from Printer
where price = (select max(price) from Printer);
--11
select AVG(speed) from PC;
--12
select avg(speed) from Laptop
where price > 1000;
--13
select avg(speed)
from PC join Product on Product.model = PC.model
where maker = 'A';
--14
select Ships.class, Ships.name, country
from Classes join Ships on Classes.class = Ships.class
where numGuns >=10;
--15
select hd
from PC
group by hd
having count(hd) >=2;
--16
select distinct A.model, B.model, A.speed, A.ram
from PC as A, PC B
where (A.speed = B.speed) and (A.ram = B.ram) and B.model < A.model;
--17
select distinct type,Laptop.model,Laptop.speed
from Laptop, PC, Product
where Product.model = Laptop.model and Laptop.speed < all(select speed from PC);
--18
select distinct maker,price
from Product join Printer on Product.model = Printer.model
where price in(select min(price) from Printer where color = 'y') and color = 'y';
--19
select  maker, avg(screen)
from Product join Laptop on Product.model = Laptop.model
group by maker;
--20
Select maker,count(maker)
from Product
where type = 'PC'
group by maker
having count(maker) >=3;
--21
select maker, max(price)
from Product join PC on Product.model = PC.model
group by maker;
--22
select speed, avg(price)
from PC
where speed > 600
group by speed;
--23
select maker
from Product join PC on Product.model = PC.model
where speed >=750
intersect
select maker
from Product join Laptop on Product.model = Laptop.model
where speed >=750;
--24
with model_price as (
    select model,price from PC
    union
    select model,price from Laptop
    union
    select model,price from Printer)
select  model from model_price
where price = (select max(price) from model_price);
--25
select distinct maker
from Product
where type = 'Printer' and
        maker in(select maker
                 from Product
                 where model in(select model
                                from PC
                                where ram = (select min(ram) from PC) and
                                      speed = (select MAX(speed)
                                               from (select speed
                                                     from PC
                                                     where ram=(select min(ram)
                                                                from PC
                                                                )
                                                     ) as max_speed
                                               )
                                )
                );
--31
select class, country
from Classes
where bore >=16;
--33
select ship
from Outcomes
where result = 'sunk' and battle = 'North Atlantic';
--38
select country
from Classes
where  type = 'bb'
intersect
select country
from Classes
where type = 'bc';
--42
select ship,battle
from Outcomes
where result = 'sunk';
--44
select name
from ships
where name like 'R%'
union
select ship
from Outcomes
where ship like 'R%';
--45
select name
from ships
where name like '% % %'
union
select ship
from Outcomes
where ship like '% % %';
--49
select name
from Ships join Classes on Ships.class = Classes.class
where bore = 16
union
select ship
from Classes join Outcomes on ship = class
where bore = 16;
--50
select distinct battle
from Ships join Outcomes on name = ship
where class = 'Kongo';
--27
select maker, avg(hd)
from PC join Product on PC.model = Product.model
where maker in(select maker from Product where type = 'Printer')
group by maker;
--28
select distinct count(maker)
from Product
where maker in (select maker from Product group by maker having count(model) = 1);
--29
select model, type
from Product
where model not like '%[^0-9]%' or model not like '%[^a-z]%';
--36
select name
from Ships
where class = name union select ship from classes,outcomes where Classes.class = Outcomes.ship;
--40
select distinct maker, max(type) as type
from product
group by maker
having count(distinct type) = 1 and count(model) > 1;
--43
select name
from battles
where datepart(yy, date) not in (select datepart(yy, date)
                                from battles join ships on datepart(yy, date) = launched
                                );
--46
select distinct ship, displacement, numguns
from Classes join Ships on Classes.class = Ships.class right join Outcomes on Classes.class=ship or name=ship
where battle  = 'Guadalcanal';

