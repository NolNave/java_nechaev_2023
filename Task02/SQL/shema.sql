drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists taxi_order;

create table client
(
    id bigserial primary key,
    first_name char(20),
    last_name  char(20),
    client_rating dec(2,1) check(client_rating > 0.0 and client_rating <5.0),
    age        integer check(age > 14 and age < 95)
);

create table driver
(
    id            bigserial primary key,
    first_name    char(20),
    last_name     char(20),
    driver_rating dec(2, 1) check (driver_rating > 0.0 and driver_rating < 5.0),
    age           integer check (age >= 21 and age < 65)
);

create table car
(
    car_brand varchar(15),
    car_number char(6),
    car_colour varchar(10)
);

create table taxi_order
(
    order_cost integer,
    distance_to_client_km integer
);

alter table client add card_number char(16) not null default '';
alter table client add phone_number varchar(11) not null default '';
alter table driver add phone_number varchar(11) not null default '';
alter table taxi_order add time_travel time(0) not null default '00:00:00';
