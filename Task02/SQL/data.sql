insert into client(first_name, last_name, client_rating, age)
values('Kirill', 'Pesterev', 4.9, 22);
insert into client(first_name, last_name, client_rating, age, card_number, phone_number)
values ('Renal', 'Gilyaziev', 2.3, 22, '1234567898765434', '89467835456'),
       ('Vladimir', 'Nechaev', 1.1, 21, '4276620044238040', '89030628013');

insert into driver(first_name, last_name, driver_rating, age)
values('Rasul', 'Gasanov', 4.5, 21),
      ('David', 'Zotov', 4.3, 22),
      ('Karim', 'Nuriaxmetov', 3.2, 25);

insert into car(car_brand, car_number, car_colour)
values('Volkswagen', 'x287cc', 'brown'),
      ('Kia Rio', 'c867xc', 'white'),
      ('Ford', 'k432cx', 'gold');

insert into taxi_order(order_cost, distance_to_client_km, time_travel)
values(200, 10,'00:15:30'),
      (415, 4, '01:02:00'),
      (300, 20, '00:30:04');

update client set phone_number = '89030617373' where id = 1;
update driver
set age = age + 1
where
    first_name = 'Rasul'
OR
    first_name = 'David'
OR
    first_name = 'Karim';
update car
set car_brand = 'Toyota',
    car_colour = 'red'
where car_number ='x287cc';