package ru.itis;

import ru.itis.dto.StudentSignUp;
import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.itis.services.StudentsService;
import ru.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.*;
import java.sql.*;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);

        studentsRepository.save(new Student("Реналь", "Гилязиев", 19));
        System.out.println(studentsRepository.findAll());
        studentsRepository.update(new Student(3L,"Кирилл","Нечаев", 26));
        System.out.println(studentsRepository.findAll());
        studentsRepository.delete(4L);
        System.out.println(studentsRepository.findAll());
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(21));
    }
}

