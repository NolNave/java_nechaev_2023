package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }
    public static User fromLineToUser(String line){

        String[] userParts = line.split("\\|");
        UUID uuid = UUID.fromString(userParts[0]);
        return new User(uuid, userParts[1], userParts[2], userParts[3], userParts[4]);
    }
    public void changeFiles(List<User> users){
        try {
            new FileWriter(fileName, false).close();
            Writer writer = new FileWriter(fileName,true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            users.forEach(x -> {
                try {
                    bufferedWriter.write(userToString.apply(x));
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            bufferedWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }





    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try(Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader)){
            String line = bufferedReader.readLine();
            while(line != null){
                users.add(fromLineToUser(line));
                line = bufferedReader.readLine();
            }
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> users = new ArrayList<>();
        try(Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader)){
            String line = bufferedReader.readLine();
            while(line != null){
                User user = fromLineToUser(line);
                if(user.getId().equals(entity.getId())){
                    user = entity;
                }
                users.add(user);
                line = bufferedReader.readLine();
            }
            changeFiles(users);
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        List<User> users = new ArrayList<>();
        try(Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                User user = fromLineToUser(line);
                if(!user.toString().equals(entity.toString())){
                    users.add(user);
                }
                line = bufferedReader.readLine();
            }
            changeFiles(users);
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void deleteById(UUID id) {
        List<User> users = new ArrayList<>();
        try(Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                User user = fromLineToUser(line);
                if(!id.equals(user.getId())){
                    users.add(user);
                }
                line = bufferedReader.readLine();
            }
            changeFiles(users);
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public User findById(UUID id) {
        try(Reader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while(line != null){
                User user = fromLineToUser(line);
                if(id.equals(user.getId())){
                    return user;
                }
                line = bufferedReader.readLine();
            }
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
