package ru.itis.taxi.repositories;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CrudRepository<ID, T> {
    List<T> findAll();

    void save(T entity);

    void update(T entity) throws FileNotFoundException;

    void delete(T entity);

    void deleteById(UUID id);

    T findById(UUID id);

}
