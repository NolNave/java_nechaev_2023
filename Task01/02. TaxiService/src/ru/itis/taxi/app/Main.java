package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.io.FileNotFoundException;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);
        usersService.signUp(new SignUpForm("Андрей", "Кузнецов",
                "andrew.kuznecov@gmail.com", "qwerty9999"));


        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        System.out.println(usersRepository.findAll());
        usersRepository.update(new User(UUID.fromString("6d2cb273-03c8-4fd7-bbbb-ba28170f375f"), "Владимир", "Нечаев",
                "vovka.nechka39gmail.com", "qwerty88"));
        usersRepository.delete(new User(UUID.fromString("5d3aaf35-5f6d-4d9c-b0aa-3bde7fa02d89"), "Андрей", "Кузнецов",
                "andrew.kuznecov@gmail.com", "qwerty9999"));
        usersRepository.deleteById(UUID.fromString("a91fd02b-7202-473f-bd80-b7a7fb9cfcc6"));
        System.out.println(usersRepository.findById(UUID.fromString("6d2cb273-03c8-4fd7-bbbb-ba28170f375f")));
        System.out.println(usersRepository.findAll());

    }
}
